Forwarded: https://github.com/MariaDB/server/pull/1556
From: =?UTF-8?q?Otto=20Kek=C3=A4l=C3=A4inen?= <otto@kekalainen.net>
Date: Wed, 30 Sep 2020 20:01:35 +0300
Subject: [PATCH] MDEV-22659: Create one single unified and optimal logrotate
 config

Replace mysql-log-rotate.sh and debian/...mysql-server.logrotate with one
new unified and well documented version.

Name is mariadb.logrotate.in as in 10.6 branch we use now the 'mariadb'
name, and use 'logrotate' to match the actual name of the utility, and
use '.in' instead of '.sh' as this is not a shell script but a template
file.

--- a/.gitignore
+++ b/.gitignore
@@ -260,7 +260,7 @@ support-files/my-innodb-heavy-4G.cnf
 support-files/my-large.cnf
 support-files/my-medium.cnf
 support-files/my-small.cnf
-support-files/mysql-log-rotate
+support-files/mariadb.logrotate
 support-files/mysql.10.0.11.spec
 support-files/mysql.server
 support-files/mysql.service
--- a/support-files/CMakeLists.txt
+++ b/support-files/CMakeLists.txt
@@ -32,6 +32,9 @@ ELSE()
   SET(MYSQLD_GROUP "mysql")
   SET(ini_file_extension "cnf")
   SET(HOSTNAME "uname -n")
+
+  # Define directly here, as cmake/install_layout.cmake has no LOGDIR to be inherited
+  SET(logdir "/var/log/mysql")
 ENDIF()
 
 # XXX: shouldn't we just have variables for all this stuff and centralise
@@ -51,9 +54,11 @@ ENDIF()
 
 IF(UNIX AND NOT WITHOUT_SERVER)
   SET(prefix ${CMAKE_INSTALL_PREFIX})
-  FOREACH(script mysqld_multi.server mysql-log-rotate binary-configure wsrep_notify)
-    CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/${script}.sh 
+  FOREACH(script mysqld_multi.server binary-configure wsrep_notify)
+    CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/${script}.sh
                    ${CMAKE_CURRENT_BINARY_DIR}/${script} @ONLY )
+    CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/mariadb.logrotate.in
+                   ${CMAKE_CURRENT_BINARY_DIR}/mariadb.logrotate @ONLY )
     INSTALL(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/${script}
       DESTINATION ${inst_location} COMPONENT Server_Scripts)
   ENDFOREACH()
@@ -95,7 +100,7 @@ IF(UNIX AND NOT WITHOUT_SERVER)
   INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/mariadb.pc DESTINATION ${INSTALL_LIBDIR}/pkgconfig COMPONENT Development)
 
   INSTALL(FILES mysql.m4 DESTINATION ${INSTALL_SHAREDIR}/aclocal COMPONENT Development)
-  
+
   SET(bindir ${INSTALL_BINDIRABS})
   SET(sbindir ${INSTALL_SBINDIRABS})
   SET(scriptdir ${INSTALL_SCRIPTDIRABS})
@@ -113,7 +118,7 @@ IF(UNIX AND NOT WITHOUT_SERVER)
   ELSE()
     SET(sysconf2dir "${sysconfdir}/mysql")
   ENDIF()
-  CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/mysql.server.sh 
+  CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/mysql.server.sh
                  ${CMAKE_CURRENT_BINARY_DIR}/mysql.server @ONLY)
   INSTALL(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/mysql.server
     DESTINATION ${inst_location} COMPONENT SupportFiles)
@@ -199,8 +204,9 @@ IF(UNIX AND NOT WITHOUT_SERVER)
   ENDIF()
 
   IF (INSTALL_SYSCONFDIR)
-    INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/mysql-log-rotate DESTINATION ${INSTALL_SYSCONFDIR}/logrotate.d
-            RENAME mysql COMPONENT SupportFiles)
+    INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/mariadb.logrotate
+            DESTINATION ${INSTALL_SYSCONFDIR}/logrotate.d
+            RENAME mariadb COMPONENT SupportFiles)
     IF(NOT HAVE_SYSTEMD)
       INSTALL(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/mysql.server
               DESTINATION  ${INSTALL_SYSCONFDIR}/init.d
--- /dev/null
+++ b/support-files/mariadb.logrotate.in
@@ -0,0 +1,56 @@
+# This is the MariaDB configuration for the logrotate utility
+#
+# Note that on most Linux systems logs are written to journald, which has its
+# own rotation scheme.
+#
+# Read https://mariadb.com/kb/en/error-log/ to learn more about logging and
+# https://mariadb.com/kb/en/rotating-logs-on-unix-and-linux/ about rotating logs.
+
+@localstatedir@/mysqld.log @logdir@/mysql.log @localstatedir@/mariadb.log @logdir@/mysql-slow.log @logdir@/mariadb-slow.log @logdir@/error.log {
+
+  # If any of the files listed above is missing, skip them silently without
+  # emitting any errors
+  missingok
+
+  # If file exists but is empty, don't rotate it
+  notifempty
+
+  # Run monthly
+  monthly
+
+  # Keep 6 months of logs
+  rotate 6
+
+  # If file is growing too big, rotate immediately
+  maxsize 500M
+
+  # If file size is too small, don't rotate at all
+  minsize 50M
+
+  # Compress logs, as they are text and compression will save a lot of disk space
+  compress
+
+  # Don't compress the log immediately to avoid errors about "file size changed while zipping"
+  delaycompress
+
+  # Don't run the postrotate script for each file configured in this file, but
+  # run it only once if one or more files were rotated
+  sharedscripts
+
+  # After each rotation, run this custom script to flush the logs. Note that
+  # this assumes that the mariadb-admin command has database access, which it
+  # has thanks to the default use of Unix socket authentication for the 'root'
+  # account used everywhere since MariaDB 10.4.
+  postrotate
+    if test -r /etc/mysql/debian.cnf
+    then
+      EXTRAPARAM='--defaults-file=/etc/mysql/debian.cnf'
+    fi
+
+    if test -x @bindir@/mariadb-admin
+    then
+      @bindir@/mariadb-admin $EXTRAPARAM --local flush-error-log \
+        flush-engine-log flush-general-log flush-slow-log
+    fi
+  endscript
+}
